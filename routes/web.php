<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/home', function () {
//     return view('home');
// });


Route::resource('trips','TripController');
Route::resource('users','UserController');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('home/created', 'HomeController@myTrips')->name('home.created');
Route::get('home/reserved', 'HomeController@myReservedTrips')->name('home.reserved');

Route::get('users/{user}/trips', 'UserController@trips')->name('users.trips');

Route::patch('trips/{trip}/reserve', 'TripController@reserve')->name('trips.reserve');
Route::patch('trips/{trip}/unreserve', 'TripController@unreserve')->name('trips.unreserve');