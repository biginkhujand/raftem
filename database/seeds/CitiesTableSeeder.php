<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert(array(
            array('city' => 'Худжанд'),
            array('city' => 'Душанбе'),
            array('city' => 'Кайраккум'),
            array('city' => 'Исфара'),
            array('city' => 'Турсунзода'),
            array('city' => 'Вахдат'),
            array('city' => 'Канибадам'),
            array('city' => 'Хорог')
        ));
    }
}
