<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(array(
            array('role' => 'administrator', 'description' => 'All privilegies'),
            array('role' => 'moderator', 'description' => 'Moderate, can add simplified trips'),
            array('role' => 'user', 'description' => 'Can add simplified trips'),
            array('role' => 'booker', 'description' => 'Statistics, reports, rates, prices'),
            array('role' => 'unauthorized', 'description' => 'Can see some pages')
        ));
    }
}
