<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->date('birth')->nullable();;
            $table->string('birth_place')->nullable();
            $table->integer('experience')->nullable();
            $table->enum('gender', ['male', 'female'])->default('male');
            $table->string('like')->nullable();
            $table->string('music')->nullable();
            $table->string('photo')->nullable();
            $table->double('rating', 1, 1)->nullable();
            $table->string('smoke')->nullable();
            $table->string('talk')->nullable();
            $table->integer('trip_count')->nullable();
            $table->integer('id_currency')->nullable();
            $table->integer('id_model')->nullable();
            $table->integer('id_review')->nullable();
            $table->integer('id_role')->default(3);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
