<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pointA');
            $table->string('pointB');
            $table->integer('price')->default(0);
            $table->integer('seat_quantity');
            $table->string('reserved')->default(0);
            $table->datetime('start_time');
            $table->enum('break', ['', 'on'])->default('')->nullable();
            $table->integer('view')->default(0);
            $table->integer('id_user')->default(0)->nullable();
            $table->integer('id_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
