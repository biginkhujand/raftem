<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservedtrip extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'id', 'id_trip', 'reserved_seat'
    ];
}
