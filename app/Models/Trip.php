<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'pointA', 'pointB', 'price', 'seat_quantity', 'start_time', 'break', 'id_user', 'reserved'
    ];

    protected $dates = ['start_time'];

    public function users()
    {
        return $this->belongsTo(\App\Models\User::class,'id_user','id');
    }

    public function scopeSearchByKeyword($query, $from, $to, $date)
    {
        if ($from!='' and $to!='' and $date!='') {
            $query->where(function ($query) use ($from, $to, $date) {
                $query->where("pointA", "LIKE","%$from%");                
                $query->where("pointB", "LIKE","%$to%");
                $query->where("start_time", "LIKE","%$date%");            
            });
        } elseif ($from!='' and $to!='') {
            $query->where(function ($query) use ($from, $to) {
                $query->where("pointA", "LIKE","%$from%");
                $query->where("pointB", "LIKE","%$to%");
            });
        } elseif ($from!='' and $date!='') {
            $query->where(function ($query) use ($from, $date) {
                $query->where("pointA", "LIKE","%$from%");
                $query->where("start_time", "LIKE","%$date%");            
            });  
        } elseif ($from!='') {
            $query->where(function ($query) use ($from) {
                $query->where("pointA", "LIKE","%$from%");
            });
        } elseif ($to!='' and $date!='') {
            $query->where(function ($query) use ($to, $date) {
                $query->where("pointB", "LIKE","%$to%");
                $query->where("start_time", "LIKE","%$date%");            
            });
        } elseif ($to!='') {
            $query->where(function ($query) use ($to) {
                $query->where("pointB", "LIKE","%$to%");
            });
        } elseif ($date!='') {
            $query->where(function ($query) use ($date) {
                $query->where("start_time", "LIKE","%$date%");            
            });
        }
        return $query;
    }
}
