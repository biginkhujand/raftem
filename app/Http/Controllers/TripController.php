<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\Trip;
use App\Models\User;
use App\Models\ReservedTrip;
use DB;

class TripController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = DB::table('cities')->pluck('city');        
        $pointA = \Request::get('pointA');
        $pointB = \Request::get('pointB');
        $start_time = \Request::get('start_time');
        if (isset($pointA) or isset($pointB) or isset($start_time)) {
            $trips = Trip::SearchByKeyword(
                $pointA,
                $pointB,
                $start_time
                )->paginate(5);
            $count = $trips->count();
        } else {
            $trips = Trip::latest()->paginate(5);
            $count = $trips->count();
        }
        return view('trips.index', compact('trips', 'count', 'cities'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = DB::table('cities')->pluck('city');
        return view('trips.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            request()->validate([
            'pointA' => 'required',
            'pointB' => 'required',
            'price' => 'required',
            'seat_quantity' => 'required',
            'start_time' => 'required',
            // 'start_time' => 'required|date_format:d/m/Y|after:tomorrow',
            ]);
        $trip = new Trip;
        $trip->fill($request->all());
        $trip->id_user = Auth::id();
        $trip->save();
        return redirect()->route('home.created')
            ->with('success', 'Поездка успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip = Trip::find($id);
        if (isset($trip)) {
            $trip->increment('view');
            return view('trips.show', compact('trip'));
        } else {
            return redirect()->route('trips.index')
                ->with('danger', 'Ошибка доступа или нет такой поездки');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities = DB::table('cities')->pluck('city');
        $trip = Trip::where('id_user', Auth::id())->find($id);
        if (isset($trip)) {
            return view('trips.edit', compact('trip', 'cities'));
        } else {
            return redirect()->route('trips.index')
                ->with('danger', 'Ошибка доступа');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'pointA' => 'required',
            'pointB' => 'required',
            'price' => 'required',
            'seat_quantity' => 'required',
            'start_time' => 'required',
            'break' => 'required',
        ]);
        $trip = Trip::where('id_user', Auth::id())->find($id);
        if (isset($trip)) {
            $trip->update($request->all());
            return redirect()->route('home.created')
                ->with('success', 'Успешное обновление поездки');
        } else {
            return redirect()->route('home.created')
                ->with('danger', 'Ошибка при обновлении');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trip = Trip::where('id_user', Auth::id())->find($id);
        if (isset($trip)) {
            $trip->delete();
            return redirect()->route('home.created')
                ->with('success', 'Поездка успешно удалена');
        } else {
            return redirect()->route('home.created')
                ->with('danger', 'Ошибка доступа');
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reserve(Request $request, $id)
    {
        $trip = Trip::where([
            ['id', $id],
            ['seat_quantity', '>', 0],
            ['id_user', '<>', Auth::id()]
        ])->find($id);
        
        if (isset($trip)) {
            $reserved = Reservedtrip::where([
                ['id', Auth::id()],
                ['id_trip', $id]
            ])->find(Auth::id());

            request()->validate([
                'reserved_seat' => 'required|integer',
            ]);

            if (isset($reserved)) {
                $reserved->increment('reserved_seat', $request->reserved_seat);
            } else {
                $reserved = new Reservedtrip;
                $reserved->id = Auth::id();
                $reserved->id_trip = $id;
                $reserved->reserved_seat = $request->reserved_seat;
                $reserved->save();
            }

            $trip->increment('reserved', $request->reserved_seat);
            $trip->decrement('seat_quantity', $request->reserved_seat);
             
            return redirect()->route('home.reserved')
                ->with('success', 'Поездка успешно забронирован');
        } else {
            return redirect()->route('trips.index')
                ->with('danger', 'Ошибка при бронировании');
        }
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unreserve(Request $request, $id)
    {
        $trip = Trip::where([
            ['id', $id],
            ['reserved', '>', 0],
            ['id_user', '<>', Auth::id()]
            ])->find($id);
        if (isset($trip)) {
            $reserved = Reservedtrip::where([
                ['id', Auth::id()],
                ['id_trip', $id],
                ['reserved_seat', '>=', 1]
            ])->find(Auth::id());
            
            request()->validate([
                'reserved_seat' => 'required|integer',
            ]);

            $reserved_seat = $request->reserved_seat;
            
            if (isset($reserved)) {
                if ($reserved->reserved_seat == $reserved_seat or
                $reserved->reserved_seat <= 1) {
                    $reserved->delete(Auth::id());
                } else {
                    $reserved->decrement('reserved_seat', $reserved_seat);
                }
            } else {
                return redirect()->route('home.reserved')
                    ->with('danger', 'Ошибка при отмене бронировании111');
            }

            $trip->decrement('reserved', $reserved_seat);
            $trip->increment('seat_quantity', $reserved_seat);

            return redirect()->route('home.reserved')
                ->with('success', 'Бронирование успешно отменено');
        } else {
            return redirect()->route('home.reserved')
                ->with('danger', 'Ошибка при отмене бронировании');
        }
    }
}
