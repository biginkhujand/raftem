<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Trip;
use App\Models\ReservedTrip;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id());
        return view('home', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myTrips()
    {
        $trips = Trip::where('id_user', Auth::id())->paginate(10);
        $count = Trip::where('id_user', Auth::id())->count();
        return view('users.created', compact('trips', 'count'))
         ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myReservedTrips()
    {
        $reserved = Reservedtrip::where('id', Auth::id())->get();
        $count = Reservedtrip::where([['id', Auth::id()],['reserved_seat', '>', 0]])->count();
        
        if (isset($reserved)) {
            $reserved = 0;            
        } else {
            foreach ($reserved as $value) {
                $reserved = $value->reserved_seat;
            } 
        }

        $trips = Trip::join('reservedtrips', function ($join) {
            $join->on('trips.id', 'reservedtrips.id_trip')
                 ->where('reservedtrips.id', Auth::id());
            })->paginate(5);
        
        return view('users.reserved', compact('trips', 'reserved', 'count'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
}
