<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Trip;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', Auth::id())->get();
        return view('users.index', ['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if (isset($user)) {
            return view('users.show', compact('user'));
        } else {
            return redirect()->route('home')
                ->with('danger', 'Ошибка доступа или нет такого пользователя');            
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function trips($id)
     {
         $trips = Trip::where('id_user', $id)->paginate(5);
         $count = Trip::where('id_user', $id)->count();
         return view('trips.index', compact('trips', 'count'))
           ->with('i', (request()->input('page', 1) - 1) * 5);
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id == Auth::id()) {
            $user = User::find($id);
            return view('users.edit', compact('user'));
        } else {
            return redirect()->route('home')
                ->with('danger', 'Ошибка доступа');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'string|max:25',
            // 'email' => 'string|email|max:25|unique:users',
            // 'password' => 'string|min:6|confirmed',
        ]);

        $user = User::where('id', Auth::id())->find($id);        
        if (isset($user)) {
            $user->update($request->all());
            return redirect()->route('home')
                ->with('success', 'Успешное обновление профиля!');
        } else {
            return redirect()->route('home')
                ->with('danger', 'Ошибка доступа');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Trip::where('id', Auth::id())->find($id);
        if (isset($user)){
            $user->update(['id_role' => 5]);
            return redirect()->route('register')
                ->with('success', 'Пользователь успешно удален');
        } else {
            return redirect()->route('home')
                ->with('danger', 'Ошибка доступа');
        }
    }
}
