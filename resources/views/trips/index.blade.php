@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Всего {{$count}}</div>

                <div class="panel-body">
                    
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @elseif (session('danger'))
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    @endif
                    
                    {!! Form::open(['method'=>'GET','route'=>'trips.index','class'=>'form-horizontal'])  !!}
                        <div class="input-group custom-search-form">
                            <span class="input-group-btn">
                                <div class="col-md-3">
                                    <select name="pointA" id="pointA" class="form-control">
                                        <option value="">Откуда</option>
                                        @foreach ($cities as $city)
                                            <option value="{{$city}}">{{$city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="pointB" id="pointB" class="form-control">
                                        <option value="">Куда</option>
                                        @foreach ($cities as $city)
                                            <option value="{{$city}}">{{$city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="date" class="form-control" name="start_time" placeholder="Дата" value="">
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary" type="submit" autofocus>
                                        Найти поездку
                                    </button>
                                </div>
                            </span>
                        </div>
                    {!! Form::close() !!}
                    <hr>
                    @foreach ($trips as $trip)
                    <form class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="pointA" class="col-md-4 control-label">Откуда</label>
                            <div class="col-md-6">
                                <input id="pointA" type="text" name="pointA" value="{{$trip->pointA}}" disabled class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pointB" class="col-md-4 control-label">Куда</label>
                            <div class="col-md-6">
                                <input id="pointB" type="text" name="pointB" value="{{$trip->pointB}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-md-4 control-label">Цена</label>
                            <div class="col-md-6">
                                <input id="price" type="text" name="price" value="{{$trip->price}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="seat_quantity" class="col-md-4 control-label">Количество мест</label>
                            <div class="col-md-6">
                                <input id="seat_quantity" type="number" name="seat_quantity" value="{{$trip->seat_quantity}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="reserved" class="col-md-4 control-label">Уже забронировано</label>
                            <div class="col-md-6">
                                <input id="reserved" type="number" name="reserved" value="{{$trip->reserved}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="start_time" class="col-md-4 control-label">Начало поездки</label>
                            <div class="col-md-6">
                                <input id="start_time" type="datetime" name="start_time" value="{{$trip->start_time->format('d m Y H:s')}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="break" class="col-md-4 control-label">Остановка</label>
                            <div class="col-md-6 switch">
                                <input id="break" type="checkbox" name="break" class="form-control"
                                <?php if ($trip->break == 'on') echo 'checked="checked"';?> disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a class="btn btn-info" href="{{ route('trips.show',$trip->id) }}">Перейти к бронированию</a>
                            </div>
                        </div>                            
                        <hr>
                    </form>
                    @endforeach
                    {{$trips->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection