@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Добавление поездки</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('trips.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('pointA') ? ' has-error' : '' }}">
                            <label for="pointA" class="col-md-4 control-label">Откуда</label>

                            <div class="col-md-6">
                                <select name="pointA" id="pointA" class="form-control" required autofocus>
                                    @foreach ($cities as $city)
                                    <option value="{{$city}}">{{$city}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('pointA'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pointA') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pointB') ? ' has-error' : '' }}">
                            <label for="pointB" class="col-md-4 control-label">Куда</label>

                            <div class="col-md-6">
                                <select name="pointB" id="pointB" class="form-control" required autofocus>
                                    @foreach ($cities as $city)
                                    <option value="{{$city}}">{{$city}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('pointB'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pointB') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Цена</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control" name="price"
                                    value="{{ old('price') }}" required>

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('seat_quantity') ? ' has-error' : '' }}">
                            <label for="seat_quantity" class="col-md-4 control-label">Количество мест</label>

                            <div class="col-md-6">
                                <input id="seat_quantity" type="number" class="form-control" name="seat_quantity" 
                                    value="{{ old('seat_quantity') }}" min="1" max="20" required>
                                
                                @if ($errors->has('seat_quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('seat_quantity') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                            <label for="start_time" class="col-md-4 control-label">Начало поездки</label>

                            <div class="col-md-6">
                            <input id="start_time" type="date" class="form-control" name="start_time"
                                value="{{ old('start_time') }}" required>
                            
                                @if ($errors->has('start_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('break') ? ' has-error' : '' }}">
                            <label for="break" class="col-md-4 control-label">Остановка</label>

                            <div class="col-md-6 switch">
                                <input id="break" type="checkbox" class="form-control" name="break">

                                @if ($errors->has('break'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('break') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Добавить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection