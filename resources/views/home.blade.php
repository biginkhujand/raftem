@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Профиль</div>

                <div class="panel-body">
                    
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @elseif (session('danger'))
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    @endif

                        <p>Имя: {{ $user->name}}</p>
                        <p>E-mail: {{ $user->email}}</p>

                    <a class="btn btn-primary" href="{{ route('users.edit', $user->id) }}">
                        Редактировать профиль
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
