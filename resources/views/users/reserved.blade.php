@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Всего забрнировано {{$count}}</div>

                <div class="panel-body">
                    
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @elseif (session('danger'))
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    @endif
                    
                    @foreach ($trips as $trip)
                    {!! Form::open(['method' => 'PATCH','route' => ['trips.unreserve', $trip->id_trip], 'class' => 'form-horizontal']) !!}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="pointA" class="col-md-4 control-label">Откуда</label>
                            <div class="col-md-6">
                                <input id="pointA" type="text" name="pointA" value="{{$trip->pointA}}" disabled class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pointB" class="col-md-4 control-label">Куда</label>
                            <div class="col-md-6">
                                <input id="pointB" type="text" name="pointB" value="{{$trip->pointB}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-md-4 control-label">Цена</label>
                            <div class="col-md-6">
                                <input id="price" type="text" name="price" value="{{$trip->price}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="seat_quantity" class="col-md-4 control-label">Количество мест</label>
                            <div class="col-md-6">
                                <input id="seat_quantity" type="number" name="seat_quantity" value="{{$trip->seat_quantity}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="reserved" class="col-md-4 control-label">Уже забронировано</label>
                            <div class="col-md-6">
                                <input id="reserved" type="number" name="reserved" value="{{$trip->reserved}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="start_time" class="col-md-4 control-label">Начало поездки</label>
                            <div class="col-md-6">
                                <input id="start_time" type="datetime" name="start_time" value="{{$trip->start_time->format('d m Y H:s')}}" disabled class="form-control">
                             </div>
                        </div>
                        <div class="form-group">
                            <label for="break" class="col-md-4 control-label">Остановка</label>
                            <div class="col-md-6 switch">
                                <input id="break" type="checkbox" name="break" class="form-control"
                                <?php if ($trip->break == 'on') { echo 'checked="checked"';}?> disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="reserved_seat" class="col-md-4 control-label">Мною забронировано ({{$trip->reserved_seat}})</label>
                        
                            <div class="col-md-6">
                                <select name="reserved_seat" id="reserved_seat" class="form-control" required>
                                    @for ($i = 1; $i <= $trip->reserved_seat; $i++)
                                        <option value="{{$i}}">{{ $i }}</option>
                                    @endfor
                                </select>
                                @if ($errors->has('reserved_seat'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reserved_seat') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a class="btn btn-info" href="{{ route('trips.show',$trip->id_trip) }}">Посмотреть</a>
                                <button type="submit" class="btn btn-danger">Отменить</button>

                            </div>
                        </div>
                        <hr>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection