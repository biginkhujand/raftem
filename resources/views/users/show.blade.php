@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Просмотр пользователя {{$user->name}}</div>

                <div class="panel-body">
                
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @elseif (session('danger'))
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    @endif

                    <form class="form-horizontal">
                        <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                <img src="{{ $user->photo }}" alt="{{ $user->name }}" name="photo">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Имя</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"
                                    value="{{ $user->name }}" disabled>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" 
                                    value="{{$user->email}}" disabled>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('birth') ? ' has-error' : '' }}">
                            <label for="birth" class="col-md-4 control-label">Дата рождения</label>

                            <div class="col-md-6">
                                <input id="birth" type="text" class="form-control" name="birth" 
                                    value="{{$user->birth}}" disabled>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('birth_place') ? ' has-error' : '' }}">
                            <label for="birth_place" class="col-md-4 control-label">Город</label>

                            <div class="col-md-6">
                                <input id="birth_place" type="text" class="form-control" name="birth_place" 
                                    value="{{$user->birth_place}}" disabled>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="gender" class="col-md-4 control-label">Пол</label>

                            <div class="col-md-6">
                                <input id="gender" type="text" class="form-control" name="gender" 
                                    value="{{$user->gender}}" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                            <label for="experience" class="col-md-4 control-label">Опыт</label>

                            <div class="col-md-6">
                                <input id="experience" type="text" class="form-control" name="experience" 
                                    value="{{$user->experience}}" disabled>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('trip_count') ? ' has-error' : '' }}">
                            <label for="trip_count" class="col-md-4 control-label">Поездки</label>

                            <div class="col-md-6">
                                <input id="trip_count" type="text" class="form-control" name="trip_count" 
                                    value="{{$user->trip_count}}" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">
                            <label for="about" class="col-md-4 control-label">О себе</label>

                            <div class="col-md-1">
                                <input id="like" type="text" class="form-control" name="like" 
                                    value="{{$user->like}}" disabled>
                            </div>

                            <div class="col-md-1">
                                <input id="music" type="text" class="form-control" name="music" 
                                    value="{{$user->music}}" disabled>
                            </div>

                            <div class="col-md-1">
                                <input id="smoke" type="text" class="form-control" name="smoke" 
                                    value="{{$user->smoke}}" disabled>
                            </div>
                            
                            <div class="col-md-1">
                                <input id="talk" type="text" class="form-control" name="talk" 
                                    value="{{$user->talk}}" disabled>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('id_model') ? ' has-error' : '' }}">
                            <label for="id_model" class="col-md-4 control-label">Авто</label>

                            <div class="col-md-6">
                                <input id="id_model" type="text" class="form-control" name="id_model" 
                                    value="{{$user->id_model}}" disabled>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('rating') ? ' has-error' : '' }}">
                            <label for="rating" class="col-md-4 control-label">Рейтинг</label>

                            <div class="col-md-6">
                                <input id="rating" type="text" class="form-control" name="rating" 
                                    value="{{$user->rating}}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">Написать</button>
                                <a class="btn btn-primary" href="{{route('trips.index')}}">Назад</a>
                            </div>
                        </div>                            
                        <hr>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection